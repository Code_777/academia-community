<!DOCKTYPE html>

<html>
    <head>
        <title>Academia Community</title>
        
        <!--Css-->
        <link href="<?php echo base_url("assets/css/bootstrap.min.css");?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/linkapp.min.css");?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/mycss.css");?>" rel="stylesheet">
        
    </head>
    <body>
        
        <div class="container">
            <div class="col-md-4 col-md-offset-4">
                <div id="signup">
                    <h2>Create a member account</h2>
                   
                    
                    <form method="post" action="<?php echo base_url("new_user");?>">
                        <div class="form-group">
                            <label class="control-label">Username</label>
                            <input type="text" class="form-control" name="username"  placeholder="" required="" value="<?php echo $user ?>">
                             <span style="color: red"><?php echo $errorusername ?></span>
                        </div>
                        <div class="form-group">
                            
                            <label class="control-label">Email</label>
                            <input type="email" class="form-control" name="useremail" placeholder="" required="" value="<?php echo $email ?>">
                             <span style="color: red"><?php echo $erroremail ?></span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Phone Number</label>
                            <input type="text" class="form-control" name="phone"  placeholder="" required="" value="<?php echo $phone ?>">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Password</label>
                            <input type="password" class="form-control" name="userpass"  placeholder="" required="">
                        </div>
                        <div class="form-group">
                             <span style="color: red"><?php echo $errorpass ?></span>
                            <label class="control-label">Confirm Password</label>
                            <input type="password" class="form-control" name="userpass2" placeholder="" required="">
                        </div>
                        <div class="  input-wrapper">
           <!--  Institution   -->  
           <?php
           $this->load->view('usersviews/list');
           ?>        
   <br>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success btn-sm form-control" value="Create account">
                        </div>
                    </form>
                    
                    <div class="text-center"> Already a member? <a href="<?php echo base_url()?>index.php/welcome/login">Sign in</a></div>
                </div>
            </div>
        </div>
        
        <!--Js-->
        
    </body>
</html>
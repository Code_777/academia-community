
<!DOCTYPE html>
<html lang="en">


<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="HTML, CSS, JS, JavaScript, jQuery plugin, image cropping, image crop, image move, image zoom, image rotate, image scale, front-end, frontend, web development">

    <title>Online Academia Community</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url("assets/css/bootstrap.min.css")?>" rel="stylesheet">

  
    <link href="<?php echo base_url("assets/css/simple-sidebar.css")?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/sbar.min.css")?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/css/font-awesome.min.css")?>" rel="stylesheet">

    <script type="text/javascript" src= "<?php echo base_url("assets/js/jquery2.min.js")?>"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -
->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

     <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
       <a href="#menu-toggle" class="btn btn-success pull-left" id="menu-toggle">&times;</a>
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <div class="profile">
                      <li>
                <div class="row ">
                <img src="<?php echo base_url()?>assets/img/<?php echo $pic;?>" class="img-circle img-thumbnail">
                </div>
               </li>
                <!-- <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myPicture">EDit Profile Picture</button> -->
            
               
               

                <!--<p>Username001</p>
                <p>School</p>
                <p>University</p>-->
                
                
                <li> 
                  <a type="button" class="btn btn-success" href="<?php echo base_url()?>index.php/welcome/edit">View Profile Details</a>
                </li>
                <li>
                   <h4><a href="schoolofengineering.html">School of Engineering</a></h4>
                </li>
                <li>
                    <h4><a href="schoolofbusiness.html">School of Business</a></h4>
                </li>
                <li>
                    <h4><a href="schoolofmedicine.html">School of Medicine and Health</a></h4>
                </li>
                <li>
                    <h4><a href="schooloflaw.html">School of Law</a></h4>
                </li>
                <li>
                    <h4><a href="schoolofeducation.html">School of Education</a></h4>
                </li>
                <li>
                    <h4><a href="schoolofecology.html">School of Environmental Studies</a></h4>
                </li>
                <li>
                   <h4><a href="schoolofarts.html">School of Arts</a></h4>
                </li>
               </li>

           <!--  <div class="row schools">
                   <h4><a href="schoolofengineering.html">School of Engineering</a></h4>
                   <h4><a href="schoolofbusiness.html">School of Business</a></h4>
                   <h4><a href="schoolofmedicine.html">School of Medicine and Health</a></h4>
                   <h4><a href="schooloflaw.html">School of Law</a></h4>
                   <h4><a href="schoolofeducation.html">School of Education</a></h4>
                   <h4><a href="schoolofecology.html">School of Environmental Studies</a></h4>
                   <h4><a href="schoolofarts.html">School of Arts</a></h4>
                   </div> -->
        
      </div>

        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
        <div class="header">
          <div class="row">
             <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class=" nav navbar-nav navbar-right">


                     <li><a href="#menu-toggle2" id="menu-toggle2">Toggle Menu</a></li>
					 <li><a href="single.php">Forum</a></li>
					 <li><a href="contact.html">Contact</a></li>
					  <li><a href="<?php echo base_url("index.php/welcome/logout")?>">sign out</a></li>
            
				 </ul>
			 </div>
                        </div>
                        </nav>
                      </div>
           

                <div class="col-md-9 row">
                    <div class="col-sm-12" id="featured">
                          <div class="page-header text-muted">
                          Featured
                          </div>
                        </div>





                        <!--/top story-->
                        
  <!-- /.modal -->
                          <div class="col-sm-7 articles">
                            <h3>This is Some Awesome Featured Content</h3>
                            <h4><span class="label label-default"></span></h4><h4>
                            <small class="text-muted">1 hour ago • <a href="single.html" class="text-muted">Read More</a></small>
                            </h4>
                          </div>
                          <div class="col-sm-2">
                            <a href="#" class="pull-right"><img src="images/icon1.png" class="img-circle"></a>
                          </div>
                      

                        <div class="col-sm-12" id="stories">
                          <div class="page-header text-muted divider">
                            Top Content
                          </div>
                        </div>


                        <!--/stories-->
                        <div class="row">
                          <div class="col-sm-10 articles">
                            <h3>Dramatically Raise the Value of Any Piece of Content</h3>
                            <h4><span class="label label-default">Username003</span></h4><h4>
                            <small class="text-muted">1 hour ago • <a href="single.html" class="text-muted">Read More</a></small>
                            </h4>
                          </div>
                          <div class="col-sm-2">
                            <a href="#" class="pull-right"><img src="images/icon1.png" class="img-circle"></a>
                          </div>
                        </div>

                        <div class="row divider">
                           <div class="col-sm-12"><hr></div>
                        </div>

                        <div class="row">
                          <div class="col-sm-10 articles">
                            <h3>14 Useful Sites for Designers</h3>
                            <h4><span class="label label-default">Username004</span></h4><h4>
                            <small class="text-muted">2 days ago • <a href="#" class="text-muted">Read More</a></small>
                            </h4>
                          </div>
                          <div class="col-sm-2">
                            <a href="#" class="pull-right"><img src="images/icon1.png" class="img-circle"></a>
                          </div>
                        </div>

                        <div class="row divider">
                           <div class="col-sm-12"><hr></div>
                        </div>

                        <div class="row">
                          <div class="col-sm-10 articles">
                            <h3>
                            <h4><span class="label label-default">Username005</span></h4><h4>
                            <small class="text-muted">3 days ago • <a href="#" class="text-muted">Read More</a></small>
                            </h4>
                          </div>
                          <div class="col-sm-2">
                            <a href="#" class="pull-right"><img src="images/icon1.png" class="img-circle"></a>
                          </div>
                        </div>

                        <div class="row divider">
                           <div class="col-sm-12"><hr></div>
                        </div>

                        <div class="row">
                          <div class="col-sm-10 articles">
                            <h3>How to: Another Fantastical Article</h3>
                            <h4><span class="label label-default">Username006</span></h4><h4>
                            <small class="text-muted">4 days ago • <a href="#" class="text-muted">Read More</a></small>
                            </h4>
                          </div>
                          <div class="col-sm-2">
                            <a href="#" class="pull-right"><img src="images/icon1.png" class="img-circle"></a>
                          </div>
                        </div>

                        <div class="row divider">
                           <div class="col-sm-12"><hr></div>
                        </div>

                        <div class="row">
                          <div class="col-sm-9 articles">
                            <h3>Another Fantastical Article of Interest</h3>
                            <h4><span class="label label-default">Username007</span></h4><h4>
                            <small class="text-muted">4 days ago • <a href="#" class="text-muted">Read More</a></small>
                            </h4>
                          </div>
                          <div class="col-sm-3">
                            <a href="#" class="pull-right"><img src="images/icon1.png" class="img-circle"></a>
                          </div>
                        </div>
                        <div class="copy">
                        <div class="col-sm-12">
                          <div class="page-header text-muted divider">
                          <a href="#"> Up Next</a>
                          </div>
                        </div>
                        </div>
                        <!--<a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a>-->
                        <div class="row" id="footer">
                          <div class="col-sm-6">

                          </div>
                          <div class="copy">
                          <div class="col-sm-6">
                            <p>
                            <a href="#" class="text-centre">&copy;OnlineAcademiaCommunity.</a>
                             
                            </p>
                          </div>
                        </div>
</div>
</div>

          <div class="col-md-3">

            <a class="twitter-timeline" data-dnt="true" href="https://twitter.com/MunenePaulKuria" data-widget-id="692801801595260929">Tweets by @MunenePaulKuria</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
          


       </div>
</div>
</div>
        
        </div>
                      <!--<hr>

                      <h3 class="text-center">
                      </h3>

                      <hr>


                   
                </div>
          </div>
               
       <div class="col-md-3">

            <a class="twitter-timeline" data-dnt="true" href="https://twitter.com/MunenePaulKuria" data-widget-id="692801801595260929">Tweets by @MunenePaulKuria</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
          


       </div>
       
    <!-- /#wrapper -->
  <!-- /.modal -->

    <!-- Loading state -->
    
    <!-- /modaL profile-->
    <div class="modal fade" id="myPicture" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
           <h3 class="modal-title" id="myModalLabel">Edit Profile Picture.</h3>
           <h4><?php echo $res ?></h4>
         </div>
         <div class="modal-body">
           <h1 id="modal-body"</h1>
            
 <form id="profilePic" enctype="multipart/form-data" method="post" action="image.php">    
  <input type="file" name="file" required>
  <p><input type="submit" name="upload" value="Upload"></p>
  </form>
            <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
           <button type="button" id="update" class="btn btn-primary">Save Changes</button>
           
       </div>
   </div>
 </div>
</div>
</div>
    
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
           <h3 class="modal-title" id="myModalLabel">My Academia Profile.</h3>
           <h4><?php echo $res ?></h4>
         </div>
         <div class="modal-body">
           <h1 id="modal-body"</h1>
            <ul>
           <!-- <li> UserName:</li>
           <li>Email: </li>
           <li>Institution : </li>
           </ul> -->
           <div id="dis">
           <div class="panel panel-primary">
          <div class="panel-heading">
        <h4 class="panel-title">Edit User Details</h4>


            </div>
          
          <div class="panel-body">
          
          <form class="form-horizontal" method="post" action="sidebar account.php" id="change">
            <div class="form-group">
              <label class="control-label" for="inputName"> <small>User Name: </small>

              </label>
              <div class="col-md-10" >
              <input class="form-control" type="text" id="username" value=" <?php echo $user; ?>">
            </div>

            </div>
            <div class="form-group">
              <label class="control-label" for="inputEmail"> <small>Phone NO_ : </small>

              </label>
              <div class="col-md-10">
              <input class="form-control"type="text" id="phone" value="<?php echo $call;?>">
            </div>
           
         </div>
            <div class="form-group">
              <label class="control-label" for="inputSub"> <small>Institution : </small>

              </label>
               <div class="col-md-10 input-wrapper">
           <!--  Institution   -->          
    <select name="inst" class="form-control" required>
                    <option value=""><?php echo $Institution; ?></option>
                        
    <option value=""></option>
  
                      
    <option value="Aberdeen College of Accountancy">Aberdeen College of Accountancy</option>
  
                      
    <option value="Adventist University of Africa">Adventist University of Africa</option>
  
                      
    <option value="Africa International University">Africa International University</option>
  
                      
    <option value="Africa Nazarene University">Africa Nazarene University</option>
  
                      
    <option value="African Institute of Research and Development Studies(AIRADS)">African Institute of Research and Development Studies(AIRADS)</option>
  
                      
    <option value="Aga Khan University">Aga Khan University</option>
  
                      
    <option value="Alphax College-Eldoret">Alphax College-Eldoret</option>
  
                      
    <option value="Amboseli institute of hosipitality & tecnology">Amboseli institute of hosipitality & tecnology</option>
  
                      
    <option value="bandari college">bandari college</option>
  
                      
    <option value="Bondo TTC">Bondo TTC</option>
  
                      
    <option value="Boston Arcadia Campus - South Africa">Boston Arcadia Campus - South Africa</option>
  
                      
    <option value="Bukura Agricultural College">Bukura Agricultural College</option>
  
                      
    <option value="Catholic University of Eastern Africa">Catholic University of Eastern Africa</option>
  
                      
    <option value="Chuka University (CU)">Chuka University (CU)</option>
  
                      
    <option value="college of estate management (CHEM) Reading UK">college of estate management (CHEM) Reading UK</option>
  
                      
    <option value="Daystar University">Daystar University</option>
  
                      
    <option value="Dedan Kimathi University of Technology (DKUT)">Dedan Kimathi University of Technology (DKUT)</option>
  
                      
    <option value="East African School of Theology">East African School of Theology</option>
  
                      
    <option value="East African University">East African University</option>
  
                      
    <option value="Egerton University (EU)">Egerton University (EU)</option>
  
                      
    <option value="Eldoret Politechnic">Eldoret Politechnic</option>
  
                      
    <option value="Embu University College">Embu University College</option>
  
                      
    <option value="EREGI TTC">EREGI TTC</option>
  
                      
    <option value="Excel Institute-Eldoret">Excel Institute-Eldoret</option>
  
                      
    <option value="GENCO Online University">GENCO Online University</option>
  
                      
    <option value="Great Lakes University of Kisumu">Great Lakes University of Kisumu</option>
  
                      
    <option value="GRETSA University">GRETSA University</option>
  
                      
    <option value="GUSII INSTITUTE">GUSII INSTITUTE</option>
  
                      
    <option value="Hekima College">Hekima College</option>
  
                      
    <option value="Inoorero University">Inoorero University</option>
  
                      
    <option value="Jaramogi Oginga Odinga University of Science and Technology">Jaramogi Oginga Odinga University of Science and Technology</option>
  
                      
    <option value="Jomo Kenyatta University of Agriculture and Technology (JKUAT)">Jomo Kenyatta University of Agriculture and Technology (JKUAT)</option>
  
                      
    <option value="Kabarak University">Kabarak University</option>
  
                      
    <option value="Karatina University">Karatina University</option>
  
                      
    <option value="KCA University">KCA University</option>
  
                      
    <option value="Kenya Aviation College">Kenya Aviation College</option>
  
                      
    <option value="Kenya Highlands Evangelical University">Kenya Highlands Evangelical University</option>
  
                      
    <option value="Kenya Institute of Management(KIM)">Kenya Institute of Management(KIM)</option>
  
                      
    <option value="Kenya Institute of Mass Communication(KIMC)">Kenya Institute of Mass Communication(KIMC)</option>
  
                      
    <option value="Kenya Institute of Professional Studies(KIPS)">Kenya Institute of Professional Studies(KIPS)</option>
  
                      
    <option value="Kenya Institute of Supplies Management">Kenya Institute of Supplies Management</option>
  
                      
    <option value="Kenya Methodist University(KeMU)">Kenya Methodist University(KeMU)</option>
  
                      
    <option value="Kenya School of Monetary Studies">Kenya School of Monetary Studies</option>
  
                      
    <option value="Kenya School of Monetary Studies">Kenya School of Monetary Studies</option>
  
                      
    <option value="Kenya Technical Teachers College (KTTC)">Kenya Technical Teachers College (KTTC)</option>
  
                      
    <option value="Kenya Utalii College">Kenya Utalii College</option>
  
                      
    <option value="Kenyatta University (KU)">Kenyatta University (KU)</option>
  
                      
    <option value="Kibabii Universtity College">Kibabii Universtity College</option>
  
                      
    <option value="Kirinyaga University College">Kirinyaga University College</option>
  
                      
    <option value="Kiriri Women�s University of Science and Technology">Kiriri Women�s University of Science and Technology</option>
  
                      
    <option value="Kisii Polytechnic">Kisii Polytechnic</option>
  
                      
    <option value="Kisii University">Kisii University</option>
  
                      
    <option value="KMTC">KMTC</option>
  
                      
    <option value="Laikipia University">Laikipia University</option>
  
                      
    <option value="Maasai Mara University">Maasai Mara University</option>
  
                      
    <option value="Machakos University College">Machakos University College</option>
  
                      
    <option value="Management University of Africa">Management University of Africa</option>
  
                      
    <option value="Marist International College">Marist International College</option>
  
                      
    <option value="Maseno University (MSU)">Maseno University (MSU)</option>
  
                      
    <option value="Masinde Muliro University of Science and Technology (MMUST)">Masinde Muliro University of Science and Technology (MMUST)</option>
  
                      
    <option value="Meru University of science  and Technology">Meru University of science  and Technology</option>
  
                      
    <option value="Moi University (MU)">Moi University (MU)</option>
  
                      
    <option value="Mt. Kenya University">Mt. Kenya University</option>
  
                      
    <option value="Multimedia University of Kenya">Multimedia University of Kenya</option>
  
                      
    <option value="Murang�a University College">Murang�a University College</option>
  
                      
    <option value="Nairobi Aviation College">Nairobi Aviation College</option>
  
                      
    <option value="Nairobi Institute of Business Studies.">Nairobi Institute of Business Studies.</option>
  
                      
    <option value="Nairobi International School of Theology">Nairobi International School of Theology</option>
  
                      
    <option value="Nakuru Elite Teachers Training College">Nakuru Elite Teachers Training College</option>
  
                      
    <option value="nakuru town campus kenya institute of crime and justice studies">nakuru town campus kenya institute of crime and justice studies</option>
  
                      
    <option value="nyandarua institute of science and technology">nyandarua institute of science and technology</option>
  
                      
    <option value="Outspan Medical College (Nyeri)">Outspan Medical College (Nyeri)</option>
  
                      
    <option value="Pan Africa Christian University">Pan Africa Christian University</option>
  
                      
    <option value="Pioneer University">Pioneer University</option>
  
                      
    <option value="Presbyterian University of East Africa">Presbyterian University of East Africa</option>
  
                      
    <option value="Pwani University (PU)">Pwani University (PU)</option>
  
                      
    <option value="Riara University">Riara University</option>
  
                      
    <option value="Rift Valley Technical Training Institute(RVTTI)">Rift Valley Technical Training Institute(RVTTI)</option>
  
                      
    <option value="Rongo University College">Rongo University College</option>
  
                      
    <option value="Scott Theological College">Scott Theological College</option>
  
                      
    <option value="South Eastern Kenya University">South Eastern Kenya University</option>
  
                      
    <option value="St. Paul�s University">St. Paul�s University</option>
  
                      
    <option value="Star Institute of Professionals, Mombasa">Star Institute of Professionals, Mombasa</option>
  
                      
    <option value="Strathmore University">Strathmore University</option>
  
                      
    <option value="Taita Taveta University College">Taita Taveta University College</option>
  
                      
    <option value="Tangaza College">Tangaza College</option>
  
                      
    <option value="Technical University of Kenya (TUK)">Technical University of Kenya (TUK)</option>
  
                      
    <option value="Technical University of Mombasa (TUM)">Technical University of Mombasa (TUM)</option>
  
                      
    <option value="temple university">temple university</option>
  
                      
    <option value="The Co-operative University College of Kenya">The Co-operative University College of Kenya</option>
  
                      
    <option value="UMMA University">UMMA University</option>
  
                      
    <option value="United States International University(USIU)">United States International University(USIU)</option>
  
                      
    <option value="University of Dar es Salaam">University of Dar es Salaam</option>
  
                      
    <option value="University of Eastern Africa, Baraton">University of Eastern Africa, Baraton</option>
  
                      
    <option value="University of Eldoret(UOE)">University of Eldoret(UOE)</option>
  
                      
    <option value="University of Kabianga">University of Kabianga</option>
  
                      
    <option value="University of Nairobi (UoN)">University of Nairobi (UoN)</option>
  
                      
    <option value="UZIMA College">UZIMA College</option>
  
                      
    <option value="Zetech University">Zetech University</option>
  
                    
                  
                </select>
            </div>
           
         </div>
       </div>
       <!-- <input type="button" id="update" class="btn btn-primary" value="Save Changes" /> -->
         </form>
         <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
           <button type="button" id="update" class="btn btn-primary">Save Changes</button>
           
       </div>
   </div>
         
     </div>
   </div>
 </diV>
  </div>
   </div>
    <!-- jQuery -->
    <script src="js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.js"></script>
    <script src="js/sidebar.min.js"></script>
<script src="js/jquery.min.js"></script>
 <script src="js/bootstrap.js"></script>
 <script src="js/wow.min.js"></script>
 <script src="js/main_ref.js"></script>
 <script src="js/ajax_obj.js"></script>
 <script src="js/responsiveslides.min.js"></script>
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="dist/cropper.min.js"></script>
  <script src="js/main.js"></script>
    <!-- Menu Toggle Script -->
    <script>
     <script >

$("#link").hover(function(){
  alert("You entered p1!");
  },
  function(){
  alert("Bye! You now leave p1!");
}); 


    </script>
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

	$("#menu-toggle2").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>

<script>
function validate(id){

    if($("#"+id).val()==null || $("#"+id).val()==""){
      var div= $("#"+id).closest("div");
      div.removeClass("has-success");
      $("#gly"+id).remove();
      div.addClass("has-error");
      div.append('<span id="gly'+id+'" class="glyphicon glyphicon-remove form-control-feedback"></span>');
      return false;
    }else{
      var div= $("#"+id).closest("div");
      div.removeClass("has-error");
      div.addClass("has-success has-feedback");
      $("#gly"+id).remove();
      div.append('<span id="gly'+id+'" class="glyphicon glyphicon-ok form-control-feedback"></span>');
      

      return true;
    }
  }
$("#update").click(function(){
  $("#dis").hide(1000);

  if(!validate("name")){
          return false;
        }
        if(!validate("email")){
          return false;
        }
        if(!validate("pass")){
          return false;
        }
        


        $("form#change").submit();



});




</script>


</body>

</html>

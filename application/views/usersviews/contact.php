<!DOCTYPE HTML>
<html>
<head>
<title>Online Academia Community Account</title>
<link href="<?php echo base_url()?>/assets/css/bootstrap.css" rel='stylesheet' type='text/css' />

<!-- Custom Theme files -->
<link rel="stylesheet" href="<?php echo base_url()?>/assets/css/touchTouch.css" type="text/css" media="all" />
<link href="<?php echo base_url()?>/assets/css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />




</head>
<body>
<!-- banner -->
<div class="banner2">
	 <div class="header">
			 <div class="logo">
				 <a href=""><img src="<?php echo base_url()?>/assets/img/logo.jpg" alt=""/></a>
			 </div>
			 <div class="top-menu">
				 <span class="menu"></span>
				 <ul class="navig">
					 <li ><a href="<?php echo base_url()?>index.php/academia">Home</a></li>
					 <li class="active"><a href="contact.html">Contact</a></li>
				 </ul>
			 </div>
			 <!-- script-for-menu -->

			 <div class="clearfix"></div>
	 </div>
</div>
<!---->
<!--contact-->
<div class="contact">
	 <div class="container">
		 <div class="main-head-section">
					<h2>Contact</h2>
		 </div>
		 <div class="contact_top">
			 <div class="col-md-8 contact_left">
			 		<h4>Below Type Here</h4>
			 		
					<form>
					  <div class="form_details">
					      <input type="text" class="text" value="Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}">
						  <input type="text" class="text" value="Email Address" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email Address';}">
						  <input type="text" class="text" value="Subject" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Subject';}">
						  <textarea value="Message" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message';}">Message</textarea>
						  <div class="clearfix"> </div>
						 <div class="sub-button">
							 <input type="submit" value="Send message">
						 </div>
					  </div>
				   </form>
			 </div>
			 <div class="col-md-4 company-right">
				 <div class="company_ad">

			      	 <address>
						 <p>email : <a href="mailto:oddmob@mail.com">oddmob@gmail.com</a></p>
						 <p>phone : +254 727 341 427</p>

					 </address>
				 </div>
			 </div>
				<div class="clearfix"> </div>
		  </div>
	 </div>
</div>
<!---->

<!---->
<div class="copywrite">
	 <div class="container">
		 <p>Copyright © 2015 Online Academia Community. All rights reserved </p>
	 </div>
</div>
<!---->
</body>
<!-- jQuery (necessary JavaScript plugins) -->
<script src="<?php echo base_url()?>/assets/js/jquery.min.js"></script>
<script src="<?php echo base_url()?>/assets/js/bootstrap.js"></script>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<script>
				$("span.menu").click(function(){
					$("ul.navig").slideToggle("slow" , function(){
					});
				});
		 </script>
		 <!-- script-for-menu -->

</html>

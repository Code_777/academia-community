<!DOCKTYPE html>

<html>
    <head>
        <title>Academia Community</title>
        
        <!--Css-->
        <?php

  $this->load->view('usersviews/head2');

  ?>
        
    </head>
    <body>
        
        <div class="container">
            <div class="col-md-4 col-md-offset-4">
                <div id="signup">
                    <h2>Edit Your Profile Details</h2>
                    <!-- <?php //echo $picerr ?> -->
                    <?php echo $error;?>
                    <div class="row">
                    <div class="col-md-5 posts-left">
                                   <img src="<?php echo base_url()?>assets/img/<?php echo $pic;?>" class="img-circle img-thumbnail">
                                   </div>
                                   <div class="col-md-7 pull-left">
                                   
                   

                        <?php echo form_open_multipart('upload/do_upload');?>

                        <input type="file" name="userfile" size="20" />

                        <br />

                        <input type="submit" value="upload" />
                    </form>
                        <br><br>

                    </div>
                        <form method="post" action="VerifyChanges">
                        <div class="form-group">
                            <label class="control-label">Username</label>
                            <input type="text" class="form-control" name="username"  placeholder="" required="" value="<?php echo $user ?>">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Phone Number</label>
                            <input type="text" class="form-control" name="phone"  placeholder="" required="" value="<?php echo $phone ?>">
                        </div>
                        <div class="  input-wrapper">
           <!--  Institution   -->  
           <?php
            $this->load->view('usersviews/list');

           ?>        
    
            </div><br>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success btn-sm form-control" value="Sava changes">
                        </div>
                    </form>
                    
                    <div class="text-center"> Go back? <a href="<?php echo base_url("index.php/welcome/login");?>">Close</a></div>
                </div>
            </div>
        </div>
        
        <!--Js-->
        
    </body>
</html>
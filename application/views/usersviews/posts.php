<html>
<head>


	<?php

  $this->load->view('usersviews/head');

  ?>
</head>
<body >

<div id="wrapper">

        <!-- Sidebar -->
  <?php 
  $this->load->view('usersviews/sidebar');

  ?>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->

  <div id="page-content-wrapper">
    <div class="header">
      <div class="logo">
				 <a href=""><img src="<?php echo base_url()?>assets/img/logo.jpg" alt=""/></a>
			</div>
    <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
          </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class=" nav navbar-nav navbar-right">

              <li><a href="#menu-toggle2" id="menu-toggle2">Toggle Menu</a></li>
					    <li><a href="<?php echo base_url()?>index.php/My_Forum">Forum</a></li>
					    <li><a href="<?php echo base_url()?>index.php/contact_us">Contact</a></li>
					    <li><a href="<?php echo base_url()?>index.php/sign_out">sign out</a></li>
				    </ul>
			    </div>
        </div>
      </nav>


      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-12" id="featured">
            <div class="page-header text-muted">Featured</div>
          </div>
        </div>
      <div class="col-sm-8" id="stories">
                          
        <div class="page-header text-muted divider">Top Content</div>
     

<!-- <h3><?php echo "Posts<br>"; echo $action_done ;?></h3> -->


<?php 
if(!empty($post)){
foreach ($post as $object) {

 echo '<div class="row">'
        .'<div class="col-sm-10 articles">'
            .'<h3>'.$object->subject.'</h3>'
            .$object->post.
            '<h4><span class="label label-default">'.$object->name.'</span></h4>'
            .'<small class="text-muted">'.$object->post_date.' <a id="'.$object->id.'"  class="text-muted" onclick="test(this.id)">Read More</a></small><br>';
  if(!empty($comments))   {       
foreach($comments as $comment) {
                            if($object->id == $comment->id) {
                                echo '<div class="media response-info">
                  <div class="media-left response-text-left">
                    
                  </div>
                  <div class="media-body response-text-right">
                  <div class="row">
                  <div class= "col-md-8">
                    ' . $comment->comment . '</div>
                    <div col-md-2>' . $comment->name . '</div>
                  </div>
                  </div>
                  <div class="clearfix"> </div>
                    </div>';
                            }

                        }
                      }


           echo  '<div id="form" class="comment_form">
              <form  method="post" action="'.base_url().'index.php/submit_comment">
              <input type="hidden" name="id" value="'.$object->id.' ">
               <input type="hidden" name="name" value="'.$object->name.' ">
                  <textarea type="text" class="form-control" name="comment" placeholder="Write your thought" required></textarea><br>
                  <button type="submit" class="btn btn-primary" value="Reply" >Reply </button>
              </form>
          </div>'

       .'</div>'

       .'<div class="col-sm-2">
         <form  method="post" action="'.base_url().'index.php/Delete_Post">
              <input type="hidden" name="id" value="'.$object->id.' ">
               <input type="hidden" name="name" value="'.$object->name.' ">
              <button type="submit" class="btn btn-mini btn-warning">Delete Post </button>
              </form>
            <a href="#" class=""><img src="'.base_url().'assets/img/'.$object->avatar.'" class="img-thumbnail img-circle"></a>
        </div>'
                              
    .'</div>'
        .'<div class="row divider">
        <div class="col-sm-12"><hr></div>
    </div>';
  }

  
  
  echo "<hr>";
  }
?>

<div class="copy">
    <div class="col-sm-12">
      <div class="page-header text-muted divider">
        <form>
          <?php 

          echo form_open('<?php echo base_url()?>/index.php/academia');
           echo form_submit('action', 'prev');
            echo form_submit('action', 'next');
            echo form_close();
          ?>
        </form>
      </div>
    </div>
</div>
       </div>           
    <div class="row" id="footer">
        <div class="col-sm-6">

        </div>
        <div class="copy">
        <div class="col-sm-6">
        <p>
        <a href="#" class="text-centre">&copy;OnlineAcademiaCommunity.</a></p>
      </div>
      </div>
    </div>
  </div>


<script>
    function hideform(){
      //$('.comment_form').hide();
      
       
    }

    function reply(id){
       $('#'+id).show();
     // alert(id);
      
    }
</script>

</body>
</html>
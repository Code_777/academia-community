
<!DOCTYPE HTML>
<html>
<head>
<!-- <?php
$this->load->view('usersviews/head');

?> -->
<link href="<?php echo base_url("assets/css/bootstrap.min.css")?>" rel="stylesheet">

   <link href="<?php echo base_url("assets/css/style.css")?>" rel="stylesheet">



</head>
<body class="container-fluid">

<div class="row">
            <div class="col-xs-12">
                <div class="banner2">
                    <div class="header">
                        <div class="logo">
                            <a href=""><img src="<?php echo base_url()?>/assets/img/logo.jpg" alt=""/></a>
                        </div>
                        <div class="top-menu">
                            <span class="menu"></span>
                            <ul class="navig">
                                <li class="active"></li>
                                <li class="active"><a href="#">Forum</a></li>
                                <li><a href="<?php echo base_url()?>index.php/academia">My Account</a></li>
                                <li><a href="<?php echo base_url()?>index.php/contact_us">Contact</a></li>
                                 <li><a href="<?php echo base_url()?>index.php/sign_out">Sign Out</a></li>
                            </ul>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
</div>

<div class="row">
    <div class="col-sm-9">
        <div class="row">
            <div class="col-xs-12">
                <div class="blog-head">
                    <h2>Forum</h2>
                </div>
                
                
            </div>
        </div>

        <div class="row">
            <div class="col-xs-5">
                <br><br>
                <h2>My Posts</h2>
            </div>
        </div>
        <?php
        foreach($posts as $object) {


            echo '<div class="row">
                                <div class="col-xs-1">
								<a href="#">
									<img class="media-object" src="images/icon1.png" alt=""/>
								</a>
								<h5><a href="#">'.$object->subject.'</a></h5>
							</div>
                            
							<div class="col-xs-9">
								<p>'.$object->post.'</p>
                                </div>
								<div class="col-md-2">
									'.$object->post_date.'
                                    </div>
                                </div>';


                        foreach($comments as $comment) {
                            if($object->id == $comment->id) {
                                echo '<div class="media response-info">
									<div class="media-left response-text-left">
									
									</div>

									<div class="col-md-10 col-md-media-body response-text-right">
										<p>' . $comment->comment . '</p>
                                        </div>
                                        <div class="col-md-2">
										' . $comment->name .'
									</div>
									
								    </div>';
                            }
                        }
							

        }
        ?>

       
        <div class="row">
            <div class="col-xs-8">
                <div class="coment-form">
                    <div id="post-status"></div>
                    <h4>Make a new Post</h4>
                    <?php echo form_open('welcome/savePost')?>
                   
                    <input type="text" id="subject" name="subject" value="Subject " onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Subject';}" required>
                   <select name='category' class="form-control" required>
                    <option value="">--Select a Category-- </option>
                       <option>School of Engineering</option>
                        <option>School of Business</option>
                         <option>School of Law</option>
                          <option>School of Medicine</option>
                           <option>School of Education</option>
                            <option>School of Arts</option>
                             <option>School of Environment</option>
                   </select><br>
                    <textarea type="text" id="comment" name="comment" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Your Comment...';}" required>Your Comment...</textarea>
                    <input id="add_comment" type="submit" value="Post Comment" >
                    <?php echo form_close()?>
                </div>


            </div>
        </div>
    </div>

    <div class="col-xs-3">

        <div class="well">
            <h4>Forum Search</h4>
            <div class="input-group">
                <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </span>
            </div>
            <!-- /.input-group -->
        </div>

        <div class="category blog-ctgry">
            <h4>Categories</h4>
            <div class="list-group">
                <a href="single.html" class="list-group-item">Cras justo odio</a>
                <a href="single.html" class="list-group-item">Dapibus ac facilisis in</a>
                <a href="single.html" class="list-group-item">Morbi leo risus</a>
                <a href="single.html" class="list-group-item">Porta ac consectetur ac</a>
                <a href="single.html" class="list-group-item">Vestibulum at eros</a>
                <a href="single.html" class="list-group-item">Cras justo odio</a>
                <a href="single.html" class="list-group-item">Cras justo odio</a>
                <a href="single.html" class="list-group-item">Cras justo odio</a>
            </div>
        </div>

        <div class="recent-posts">
            <h4>Recent posts</h4>
            <div class="recent-posts-info">
                <div class="posts-left ">
                    <a href="single.html"> </a>
                </div>

                <label>August 17, 2015</label>
                <h5><a href="single.html">FINIBUS BONORUM</a></h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing incididunt.</p>
                <a href="single.html" class="btn btn-primary hvr-rectangle-in">Read More</a>

                <div class="clearfix"> </div>
            </div>
            <div class="recent-posts-info">

                <a href="single.html"></a>


                <label>August 17, 2015</label>
                <h5><a href="single.html">FINIBUS BONORUM</a></h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing incididunt.</p>
                <a href="single.html" class="btn btn-primary hvr-rectangle-in">Read More</a>
            </div>

            <div class="clearfix"> </div>
        </div>

    </div>
</div>



</body>
</html>

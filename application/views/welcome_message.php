<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to Academia Community</title>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/mycss.css">
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
<script >
	$(function () {
  $('[data-toggle="popover"]').popover()
})
	</script>

</head>
<body>

<div id="container">
	<div class="row">
		<div class="col-md-8 pull-left">
			<img src="<?php echo base_url()?>assets/img/logo.jpg" />
		</div>

		<div class="col-md-2">
			<a href="<?php echo base_url()?>index.php/sign_up" type="button"  class="btn btn-success">Create an account</a>
		</div>
		<div class="col-md-2">
			<a href="<?php echo base_url()?>index.php/sign_in" type="button" class="btn btn-success">Login</a>
		</div>
		
	</div>
	<div class="col-md-7 pull-right">
<div id="mySlide" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators">
		<li data-target="#mySlide" data-slide-to="0" class="active"></li>
		<li data-target="#mySlide" data-slide-to="1"> </li>
		<li data-target="#mySlide" data-slide-to="2"> </li>
		<li data-target="#mySlide" data-slide-to="3"> </li>
	</ol>
 
	<div class="carousel-inner" role="listbox">
		<div class="active item">
			<img src="<?php echo base_url()?>assets/img/banner.jpg" class="img-responsive img-rounded" >
			<div class="carousel-caption">
				<h3>Welcome to the first Kenyan Online Academia Community</h3>
							
							
			</div>
		</div>
		<div class="item back">
			<img src="<?php echo base_url()?>assets/img/banner.jpg" class="img-responsive img-rounded" >
			<div class="carousel-caption">
			<h3>Start by signing up and giving yourself an identity in the community</h3>
			</div>
		</div>
			<div class="item">
			<img src="<?php echo base_url()?>assets/img/banner.jpg" class="img-responsive img-rounded" >
			<div class="carousel-caption">
			<h3>Follow by selecting your school of interest in order to follow the school's forum.</h3>

			</div>
		</div>
		<div class="item">
			<img src="<?php echo base_url()?>assets/img/banner.jpg" class="img-responsive img-rounded ">
			<div class="carousel-caption">
				<h3> Post your questions, reply to others posts, let the discussion flow </h3>

			</div>
		</div>
	</div>

	<a class="left carousel-control " href="#mySlide" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true">
		</span>
		<span class="sr-only">Previous</span>
	</a>
		<a class="right carousel-control " href="#mySlide" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true">
		</span>
		<span class="sr-only">Next</span>
	</a>



</div>
</div>
</div>



</body>
</html>
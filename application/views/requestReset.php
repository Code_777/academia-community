<!DOCKTYPE html>

<html>
    <head>
        <title>Academia Community</title>
        
        <!--Css-->
        <link href="<?php echo base_url("assets/css/bootstrap.min.css");?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/linkapp.min.css");?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/mycss.css");?>" rel="stylesheet">


<link href="<?php echo base_url("assetscss/style.css")?>" rel='stylesheet' type='text/css' />
        
    </head>
    <body>
      
        <div class="container">
            <div class="col-md-4 col-md-offset-4">
                <div id="login">
                    <h2>Reset Your account Password</h2>
                    <span style="color: red"><?php echo $error ?></span>
                    <span style="color: green"><?php echo $succ ?></span>
                    <form method="post" action="<?php echo base_url("index.php/VerifyReset");?>">
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input type="email" class="form-control" name="useremail" id="useremail" placeholder="Enter your email" required="">
                        </div>
                        
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary btn-sm form-control" value="Send">
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
        
        <!--Js-->
        
    </body>
</html>
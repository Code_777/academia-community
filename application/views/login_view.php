<!DOCKTYPE html>

<html>
    <head>
        <title>Academia Community</title>
        
        <!--Css-->
        <link href="<?php echo base_url("assets/css/bootstrap.min.css");?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/linkapp.min.css");?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/mycss.css");?>" rel="stylesheet">
    </head>
    <body>
      
        <div class="container">
            <div class="col-md-4 col-md-offset-4">
                <div id="login">
                    <h2>Sign in into your account</h2>
                    <span style="color: red"><?php echo $error ?></span>
                    <form method="post" action="<?php echo base_url("index.php/home");?>">
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input type="email" class="form-control" name="useremail" id="useremail" value="<?php echo $email;?>" placeholder="" required="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Password <a href="<?php echo base_url("index.php/welcome/reset");?>">Forgot password?</a></label>
                            <input type="password" class="form-control" name="userpass" id="userpass" value="<?php echo $password;?>" placeholder="" required="">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success btn-sm form-control" value="Sign in">
                        </div>
                    </form>
                    
                    <div class="text-center"> Not a member? <a href="<?php echo base_url("index.php/welcome/signup");?>">Sign up</a></div>
                </div>
            </div>
        </div>
        
        <!--Js-->
        
    </body>
</html>
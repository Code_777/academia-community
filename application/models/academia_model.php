<?php 
	/**
	* 
	*/
	class Academia_model extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		public function getAdminPost($email){
			return $this->db->query("select * from academia_post where email='$email'")->result();
		}

		public function getComents(){
			return $this->db->query("select * from post_comments")->result();
		}
		public function getPage($page){
			$offset = 10*$page;
			$limit = 10;
			$sql = "select * from posts limit $offset ,$limit";
			$result = $this->db->query($sql)->result();
			return $result;
		}
		public function getAllPosts(){
			$query = $this->db->query('select * from academia_post');
			return $query->num_rows();
		}

		public function getPosts($offset){
			$query = $this->db->query('select * from academia_post order by id DESC limit 5 offset '.$offset);

			if($query->num_rows() > 0)
			{
				return $query->result();
			}else{
				return NULL;
			}
		}
		
		public function getE($offset){
			$sch='School of Engineering';
			$this->db->from('academia_post');
        $this->db->where('category', $sch);
       
        $this->db->limit(5);
        $this->db->offset($offset);

        $query = $this->db->get();


			if($query->num_rows() > 0)
			{
				return $query->result();
			}else{
				return NULL;
			}
		}
		public function delPost($id,$name){
			$this->db->from('academia_post');
		
			$this->db->where('id',$id);
			$this->db->where('name',$name);
			$this->db->delete();
		}
		public function delComm($id,$name){
			$this->db->from('post_comments');
		
			$this->db->where('id',$id);
			$this->db->where('name',$name);
			$this->db->delete();
		}


		public function getB($offset){
			$sch='School of Business';
			$this->db->from('academia_post');
        $this->db->where('category', $sch);
       
        $this->db->limit(5);
        $this->db->offset($offset);

        $query = $this->db->get();


			if($query->num_rows() > 0)
			{
				return $query->result();
			}else{
				return NULL;
			}
		}
		public function getA($offset){
			$sch='School of Arts';
			$this->db->from('academia_post');
        $this->db->where('category', $sch);
       
        $this->db->limit(5);
        $this->db->offset($offset);

        $query = $this->db->get();

			if($query->num_rows() > 0)
			{
				return $query->result();
			}else{
				return NULL;
			}
		}
		public function getL($offset){

			$sch='School of Law';
		//	$this->db->where('category',$sch);
			//$query = $this->db->query('select * from academia_post limit 5 offset '.$offset);
			// $this->db->select('user_id,username, email, password,phone,school,avatar');
        $this->db->from('academia_post');
        $this->db->where('category', $sch);
       
        $this->db->limit(5);
        $this->db->offset($offset);

        $query = $this->db->get();

			if($query->num_rows() > 0)
			{
				return $query->result();
			}else{
				return NULL;
			}
		}
public function getM($offset){
	$sch='School of Medicine';
			$this->db->from('academia_post');
        $this->db->where('category', $sch);
       
        $this->db->limit(5);
        $this->db->offset($offset);

        $query = $this->db->get();


			if($query->num_rows() > 0)
			{
				return $query->result();
			}else{
				return NULL;
			}
		}
public function getEd($offset){
	$sch='School of Education';
			$this->db->from('academia_post');
        $this->db->where('category', $sch);
       
        $this->db->limit(5);
        $this->db->offset($offset);

        $query = $this->db->get();

			if($query->num_rows() > 0)
			{
				return $query->result();
			}else{
				return NULL;
			}
		}
		public function getEn($offset){
		$sch='School of Environment';
			$this->db->from('academia_post');
        $this->db->where('category', $sch);
       
        $this->db->limit(5);
        $this->db->offset($offset);

        $query = $this->db->get();


			if($query->num_rows() > 0)
			{
				return $query->result();
			}else{
				return NULL;
			}
		}




		public function savePost($data){
			$this->db->insert('academia_post', $data);
		}

		public function saveComment($data){
			$this->db->insert('post_comments', $data);
		}



		public function updatePic($email,$pic){
			
			$this->db->where('email',$email);
			$this->db->update('academia_post',$pic);
		}

	}
?>
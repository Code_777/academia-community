<?php

Class Usermodel extends CI_Model {

    function login($useremail, $password) {
        $this->db->select('user_id,username, email, password,phone,school,avatar');
        $this->db->from('users');
        $this->db->where('email', $useremail);
        $this->db->where('password', ($password));
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }
    function checkemail($email){

        $this->db->select('user_id');
        $this->db->from('users');
        $this->db->where('email',$email);
         $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }

    }
     function checkusername($username){

        $this->db->select('user_id');
        $this->db->from('users');
        $this->db->where('username',$username);
         $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }

    }
    function adduser($userinfo){
       $res= $this->db->insert('users',$userinfo);
       if($res){
        return true;
       }else{
        return false;
       }
    }
    function update($email,$data){
        $this->db->where('email',$email);
       $up= $this->db->update('users',$data);
       if($up){
        return true;
       }else{
        return false;
       }

    }
    function updatePic($email,$pic){
        $this->db->where('email',$email);
        $this->db->update('users',$pic);
    }
    function getPic($email){
        $this->db->select('avatar');
        $this->db->where('email',$email);
        $this->db->from('users');
     return  $this->db->get()->result();

    }

}

















?>
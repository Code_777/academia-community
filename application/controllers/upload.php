<?php

class Upload extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}



	function do_upload()
	{
		$config['upload_path'] = './assets/img/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			 if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');

			$error = array(
				 'user'=>$session_data['username'],
                'phone'=>$session_data['phone'],
                'inst'=>$session_data['inst'],
				'pic'=>$session_data['pic'],
				'error' => $this->upload->display_errors());

			$this->load->view('usersviews/editDetails', $error);
		}
		}
		else
		{
			//$data = array('upload_data' => $this->upload->data());
			$data= $this->upload->data();
			 if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $email= $session_data['memberEmail'];
            $pic=array( 
            	'avatar'=>$data['file_name']);
            $this->usermodel->updatePic($email,$pic);
            $this->academia_model->updatePic($email,$pic);
            $session_data['pic']=$data['file_name'];

			$error= array(
				 'user'=>$session_data['username'],
                'phone'=>$session_data['phone'],
                'inst'=>$session_data['inst'],
				'error'=>'Successfully Upadated your Profile picture',
				'pic'=> $session_data['pic']
				);

			$this->showPosts($error);
		}
		}
	}
	public function showPosts($data2){
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        //$this->load->model('academia_model');

        $data['action'] = $this->input->get('action');
        $data+=$data2;
        $count = $this->academia_model->getAllPosts();
        if (isset($_SESSION['offet'])) {

            $offset=0;
            if ($data['action']=='next') {

                if(($_SESSION['offet'] != $count) || ($_SESSION['offet'] > $count )){
                    $_SESSION['offet']+=5;
                    $offset = $_SESSION['offet'];
                }else{
                    $_SESSION['offet']=$count;
                    $offset = $_SESSION['offet'];
                }

            }else if ($data['action']=='prev') {
                if($_SESSION['offet'] != 0){
                    $_SESSION['offet']-=5;
                    $offset = $_SESSION['offet'];
                }else{
                    $_SESSION['offet']=0;
                    $offset = $_SESSION['offet'];
                }
                
            }else{
                $_SESSION['offet']=0;
                $offset = $_SESSION['offet'];
            }

        }else{
            $_SESSION['offet']=0;
            $offset = $_SESSION['offet'];
        }

        $data['action_done'] = $_SESSION['offet'];

        $data['post'] = $this->academia_model->getPosts($offset);

        $this->load->view('usersviews/posts',$data);
    }
}
?>
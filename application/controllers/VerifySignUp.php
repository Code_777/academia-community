<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class VerifySignUp extends CI_Controller {

   function __construct() {
        parent::__construct();
    }

    function index() {

   
        //This method will have the credentials validation
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('phone', 'Phone Number', 'trim|required');
        $this->form_validation->set_rules('userpass', 'Password', 'trim|required');
        $this->form_validation->set_rules('userpass2', 'Confirm Password', 'trim|required');
        $this->form_validation->set_rules('useremail', 'Email', 'trim|valid_email|required');
        $pass= $this->input->post('userpass');
        $pass2= $this->input->post('userpass2');
        $inst= $this->input->post('inst');
         $useremail = $this->input->post('useremail');
            $user=$this->input->post('username');
            $phone= $this->input->post('phone');
            $validusername = $this->usermodel->checkusername($user);

     
             $result = $this->usermodel->checkemail($useremail);
        if ($result) {
            
             $erroremail= "Email already exists,please login or reset your password";
           
            $data = array(
                'user'=>$user,
                'phone'=>$phone,
                'inst'=>$inst,
            'email' => $useremail,
            'password' => "",
            'erroremail'=>$erroremail,
            'errorpass'=>'',
            'errorusername'=>''
        );
        $this->load->view('singup_view', $data);
        
    }

        elseif($validusername) {
            
             $errorusername= "Username already exists,please use another username";
           
            $data = array(
                'user'=>$user,
                'phone'=>$phone,
                'inst'=>$inst,
            'email' => $useremail,
            'password' => "",
            'erroremail'=>'',
            'errorpass'=>'',
            'errorusername'=>$errorusername
        );
        $this->load->view('singup_view', $data);
    
       }elseif ($pass != $pass2) {
         $errorpass= "Password Mismatch, passwords must match.";
           
            $data = array(
                'user'=>$user,
                'phone'=>$phone,
                'inst'=>$inst,
            'email' => $useremail,
            'password' => "",
            'errorpass'=>$errorpass,
            'erroremail'=>'',
            'errorusername'=>''
        );
        $this->load->view('singup_view', $data);
           

        
           
            //$this->load->view('login_view');
        } else
         {
           $pic="icon1.png";

            $userinfo = array('username'=>$user,
            'password'=>md5($pass),
            'email'=>$useremail,
            'phone'=>$phone,
            'school'=>$inst,
            'avatar'=>$pic );
           $add= $this->usermodel->adduser($userinfo);
           if($add){


            $data = array(
            'email' => $useremail,
            'password' => "",
            'error'=>''
        );
        $this->load->view('login_view', $data);
        }
     }
    }

    
}

?>
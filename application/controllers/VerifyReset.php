 <?php
class VerifyReset extends CI_Controller{



    function index(){

  $this->load->library('form_validation');

        $this->form_validation->set_rules('useremail', 'User email', 'trim|required');
        $email= $this->input->post('useremail');
        if ($this->form_validation->run() == FALSE) {
        $data= array(
            'error'=>'Please Enter a valid email address',
            'succ'=>''

            );
        $this->load->view('requestReset',$data);
      }
        $result = $this->usermodel->checkemail($email);
        if ($result) {
            $data= array(
            'error'=>'',
            'succ'=>'Successful, a link has been sent to your email.'

            );
        $this->load->view('requestReset',$data);
        }else{
            $data= array(
            'error'=>'The email is not registered,please recheck or <a href="welcome">sign up</a>',
            'succ'=>''

            );
        $this->load->view('requestReset',$data);

        }

    }


    }
<?php

class Welcome extends CI_Controller {

    function index(){
        $this->load->view('welcome_message');
    }

  public  function login() {
        
if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data= array();
            $data = array(
                'memberEmail'=>$session_data['memberEmail'],



                );
             $email=$session_data['memberEmail'];
            
           $pic= $this->usermodel->getPic($email);
           foreach ($pic as $pic2) {
               # code...
           $data['pic']= $pic2->avatar;
           }
            $data['comments']=$this->academia_model->getComents();

             $pass= $this->showPosts();
             $this->load->model('academia_model');
        $data['post'] = $this->academia_model->getPosts($pass);
        
            $this->load->view('usersviews/posts',$data);
        } else {
            //If no session, redirect to login page
            //redirect('login_view', 'refresh');
             $data = array(
                'error'=>"",
            'email' => "",
            'password' => ""
        );
        $this->load->view('login_view', $data);
   

           // $this->load->view('login_view');
        }
    }
  public  function logout() {
        $this->session->unset_userdata('logged_in');
        session_destroy();
        //redirect('login_view', 'refresh');
        
        $this->load->view('welcome_message');
    }
  public  function signup(){
    	 $data = array(
                'user'=>'',
                'phone'=>'',
                'inst'=>'',
            'email' => '',
            'password' => "",
            'errorpass'=>'',
            'erroremail'=>'',
            'errorusername'=>''
        );
        $this->load->view('singup_view', $data);

    }
   public function reset(){

       
        $data= array(
            'error'=>'',
            'succ'=>''

            );
        $this->load->view('requestReset',$data);
     }
   public  function edit(){
        if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data = array(
                'error'=>'',
                'user'=>$session_data['username'],
                
                'phone'=>$session_data['phone'],
                'inst'=>$session_data['inst']


                );
            $email=$session_data['memberEmail'];
            
           $pic= $this->usermodel->getPic($email);
           foreach ($pic as $pic2) {
               # code...
           $data['pic']= $pic2->avatar;
           }
           
            $this->load->view('usersviews/editDetails',$data);
     }else{
         $data = array(
                'error'=>"Login first to able to edit your profile",
            'email' => "",
            'password' => ""
        );
        $this->load->view('login_view', $data);
        


     }

}


     public function showAdminPost(){
        $this->load->model('academia_model');
        $data['action']=$this->input->get('action');
       
        
         $session_data = $this->session->userdata('logged_in');

        $email=$session_data['memberEmail'];


        $data['posts'] = $this->academia_model->getAdminPost($email);
        $data['comments']=$this->academia_model->getComents();
        $this->load->view('usersviews/forums',$data);
    }
   
    public function saveComment(){
        $this->load->model('academia_model');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('comment', 'Comment', 'required');
        $session_data = $this->session->userdata('logged_in');
        $data['forname']='';
        $data = array(
            'id'=>$this->input->post('id'),
            'name'=>$session_data['username'],
            'comment'=>$this->input->post('comment'),
            'post_data'=>date('Y-m-d H:i:s')
        );

        $this->academia_model->saveComment($data);
        $this->login();

    }
    public function delPost(){
         $this->academia_model->delComm($this->input->post('id'),$this->input->post('name'));
        $this->academia_model->delPost($this->input->post('id'),$this->input->post('name'));
        $this->login();
    }
    public function savePost(){
        $this->load->model('academia_model');
        $this->load->library('form_validation');
        
       
        $this->form_validation->set_rules('subject', 'Subject', 'required');
        $this->form_validation->set_rules('comment', 'Comment', 'required');

        $data['formname']='comment form';
        if($this->form_validation->run()==FALSE){
            $this->load->view('usersviews/forums', $data);
        }else{
             $session_data = $this->session->userdata('logged_in');

            $data = array(
                'name'=>$session_data['username'],
                'email'=>$session_data['memberEmail'],
                'subject'=>$this->input->post('subject'),
                'post'=>$this->input->post('comment'),
                'category'=>$this->input->post('category'),
                'post_date'=>date('Y-m-d H:i:s')
            );
            $email=$session_data['memberEmail'];
            
           $pic= $this->usermodel->getPic($email);
           foreach ($pic as $pic2) {
               # code...
           $data['avatar']= $pic2->avatar;
           }

            $this->academia_model->savePost($data);
            $this->login();

        }
    }
    public function enginer(){
       
        $pass= $this->showPosts();
    $session_data = $this->session->userdata('logged_in');
     $email=$session_data['memberEmail'];
            
           $pic= $this->usermodel->getPic($email);
           foreach ($pic as $pic2) {
               # code...
           $data['pic']= $pic2->avatar;
       }
        //$data['pic']= $session_data['pic'];
        $data['sch']='School of Engineering';
         $data['post'] = $this->academia_model->getE($pass);
        $data['comments']=$this->academia_model->getComents();
        $this->load->view('usersviews/engineering',$data);


    }
     public function edu(){
        $pass= $this->showPosts();
         $session_data = $this->session->userdata('logged_in');
       // $data['pic']= $session_data['pic'];
         $email=$session_data['memberEmail'];
            
           $pic= $this->usermodel->getPic($email);
           foreach ($pic as $pic2) {
               # code...
           $data['pic']= $pic2->avatar;
       }
        $data['sch']='School of Education';
         $data['post'] = $this->academia_model->getEd($pass);
        $data['comments']=$this->academia_model->getComents();
        $this->load->view('usersviews/edu',$data);


    }
     public function business(){
        $pass= $this->showPosts();
    $session_data = $this->session->userdata('logged_in');
       // $data['pic']= $session_data['pic'];
    $email=$session_data['memberEmail'];
            
           $pic= $this->usermodel->getPic($email);
           foreach ($pic as $pic2) {
               # code...
           $data['pic']= $pic2->avatar;
       }
        $data['sch']='School of Business';
         $data['post'] = $this->academia_model->getB($pass);
        $data['comments']=$this->academia_model->getComents();
        $this->load->view('usersviews/business',$data);


    }
     public function law(){
        $pass= $this->showPosts();
        $session_data = $this->session->userdata('logged_in');
       // $data['pic']= $session_data['pic'];
        $email=$session_data['memberEmail'];
            
           $pic= $this->usermodel->getPic($email);
           foreach ($pic as $pic2) {
               # code...
           $data['pic']= $pic2->avatar;
       }
        $data['sch']='School of Law';
         $data['post'] = $this->academia_model->getL($pass);
        $data['comments']=$this->academia_model->getComents();
        $this->load->view('usersviews/law',$data);


    }
     public function med(){
        $pass= $this->showPosts();
        $session_data = $this->session->userdata('logged_in');
        //$data['pic']= $session_data['pic'];
        $email=$session_data['memberEmail'];
            
           $pic= $this->usermodel->getPic($email);
           foreach ($pic as $pic2) {
               # code...
           $data['pic']= $pic2->avatar;
       }
        $data['sch']='School of Medicine';
         $data['post'] = $this->academia_model->getM($pass);
        $data['comments']=$this->academia_model->getComents();
        $this->load->view('usersviews/med',$data);


    }
     public function enviro(){
        $pass= $this->showPosts();
        $session_data = $this->session->userdata('logged_in');
       //ic']= $session_data['pic'];
        $email=$session_data['memberEmail'];
            
           $pic= $this->usermodel->getPic($email);
           foreach ($pic as $pic2) {
               # code...
           $data['pic']= $pic2->avatar;
       }
        $data['sch']='School of Environment';
         $data['post'] = $this->academia_model->getEn($pass);
        $data['comments']=$this->academia_model->getComents();
        $this->load->view('usersviews/enviro',$data);


    }
     public function arts(){

        $pass= $this->showPosts();
        $session_data = $this->session->userdata('logged_in');
       // $data['pic']= $session_data['pic'];jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj
        $email=$session_data['memberEmail'];
            
           $pic= $this->usermodel->getPic($email);
           foreach ($pic as $pic2) {
               # code...
           $data['pic']= $pic2->avatar;
       }
        $data['sch']='School of Arts';
         $data['post'] = $this->academia_model->getA($pass);
        $data['comments']=$this->academia_model->getComents();
        $this->load->view('usersviews/arts',$data);


    }
    public function showPosts(){
        if($this->session->userdata('logged_in')){
        $this->load->library('form_validation');
        //$this->load->model('academia_model');

        $data['action'] = $this->input->get('action');
       
        $count = $this->academia_model->getAllPosts();
        if (isset($_SESSION['offet'])) {

            $offset=0;
            if ($data['action']=='next') {

                if(($_SESSION['offet'] <= $count )){
                    $i = $count-$_SESSION['offet'];

                    if($i > 5){
                        $_SESSION['offet']+=5;
                        $offset = $_SESSION['offet'];
                    }else{
                        $_SESSION['offet']+=1;
                        $offset = $_SESSION['offet'];
                        $_SESSION['offet']-=1;
                    }


                }else{
                    $_SESSION['offet']=$count-5;
                    $offset = $_SESSION['offet'];
                }

            }else if ($data['action']=='prev') {
                if($_SESSION['offet'] != 0 || $_SESSION['offet']<0){
                    $_SESSION['offet']-=5;
                    $offset = $_SESSION['offet'];
                }else{
                    $_SESSION['offet']=0;
                    $offset = $_SESSION['offet'];
                }
                
            }else{
                $_SESSION['offet']=0;
                $offset = $_SESSION['offet'];
            }

        }else{
            $_SESSION['offet']=0;
            $offset = $_SESSION['offet'];
        }


        $data['action_done'] = $_SESSION['offet'];
        $pass= $data['action_done'];

        

       return $pass;
   }else{
    $this->login();
   }
    }
    public function checkSession(){
        if($this->session->userdata('logged_in')){
            return true;
        }else{
            $this->login();
        }
    }
     public  function contactus() {
       
        //redirect('login_view', 'refresh');
        
        $this->load->view('usersviews/contact');
    }

}

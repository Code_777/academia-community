<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class VerifyLogin extends CI_Controller {

   function __construct() {
        parent::__construct();
    }

    function index() {

   
        //This method will have the credentials validation
        $this->load->library('form_validation');

        $this->form_validation->set_rules('useremail', 'User email', 'trim|required');
        //$login= array();
        $this->form_validation->set_rules('userpass', 'Password', 'trim|required');
        $password=$this->input->post('userpass');


        if ($this->form_validation->run() == FALSE) {
            //Field validation failed.  User redirected to login page
            $error= "Invalid email or password,please try again  WWW";
            $useremail = $this->input->post('useremail');
            $data = array(
            'email' => $useremail,
            'password' => "",
            'error'=>$error
        );
        $this->load->view('login_view', $data);
            //$this->load->view('login_view');
        }else{

        //Field validation succeeded.  Validate against database
        $useremail = $this->input->post('useremail');

        //query the database
        $result = $this->usermodel->login($useremail, md5($password));
        if ($result) {
            $sess_array = array();
            foreach ($result as $row) {
                $sess_array = array(
                    'memberId' => $row->user_id,
                    'memberEmail' => $row->email,
                    
                    'username'=>$row->username,
                    'inst'=>$row->school,
                    'phone'=>$row->phone
                    
                );
                $this->session->set_userdata('logged_in', $sess_array);
               
            }
            // $email=$session_data['memberEmail'];
            
           $pic= $this->usermodel->getPic($useremail);
           foreach ($pic as $pic2) {
               # code...
           $sess_array['pic']= $pic2->avatar;
           }

             $this->showPosts($sess_array);
        } else {
             $error= "Invalid email or password,please try again its u";
            $useremail = $this->input->post('useremail');
            $data = array(
            'email' => $useremail,
            'password' => "",
            'error'=>$error
        );
        $this->load->view('login_view', $data);
           
        }
    }
}


public function showPosts($data2){
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        //$this->load->model('academia_model');

        $data['action'] = $this->input->get('action');
        $data+=$data2;
        $count = $this->academia_model->getAllPosts();
        if (isset($_SESSION['offet'])) {

            $offset=0;
            if ($data['action']=='next') {

                if(($_SESSION['offet'] != $count) || ($_SESSION['offet'] > $count )){
                    $_SESSION['offet']+=5;
                    $offset = $_SESSION['offet'];
                }else{
                    $_SESSION['offet']=$count;
                    $offset = $_SESSION['offet'];
                }

            }else if ($data['action']=='prev') {
                if($_SESSION['offet'] != 0){
                    $_SESSION['offet']-=5;
                    $offset = $_SESSION['offet'];
                }else{
                    $_SESSION['offet']=0;
                    $offset = $_SESSION['offet'];
                }
                
            }else{
                $_SESSION['offet']=0;
                $offset = $_SESSION['offet'];
            }

        }else{
            $_SESSION['offet']=0;
            $offset = $_SESSION['offet'];
        }

        $data['action_done'] = $_SESSION['offet'];

        $data['post'] = $this->academia_model->getPosts($offset);

        $this->load->view('usersviews/posts',$data);
    }
}